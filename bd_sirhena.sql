-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 15-06-2020 a las 07:09:43
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bd_sirhena`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(68, '2014_10_12_000000_create_users_table', 1),
(69, '2017_06_05_205544_create_usuarios_table', 1),
(70, '2018_06_07_034938_create_curricula_table', 1),
(71, '2019_08_19_000000_create_failed_jobs_table', 1),
(72, '2020_06_05_344841_create_categorias_table', 1),
(73, '2020_06_06_010038_create_ofertas_table', 1),
(74, '2020_06_06_032306_create_formacions_table', 1),
(75, '2020_06_06_032923_create_experiencias_table', 1),
(76, '2020_06_10_182616_create_requisitos_table', 1),
(77, '2020_06_11_204119_create_postulacions_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_categorias`
--

CREATE TABLE `tbl_categorias` (
  `id` int(10) UNSIGNED NOT NULL,
  `cat_nombre` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `cat_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `cat_usuario` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tbl_categorias`
--

INSERT INTO `tbl_categorias` (`id`, `cat_nombre`, `cat_description`, `cat_usuario`, `created_at`, `updated_at`) VALUES
(1, 'Administracion', 'Parte administrativa de la una', 3, '2020-06-15 09:59:19', '2020-06-15 10:00:19'),
(2, 'Informatica', 'Tecnologias de informacion', 3, '2020-06-15 10:00:05', '2020-06-15 10:00:05'),
(3, 'Desarrollo web', 'Programacion de aplicaciones web', 2, '2020-06-15 10:09:30', '2020-06-15 10:09:30');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_curriculum`
--

CREATE TABLE `tbl_curriculum` (
  `cur_id` int(10) UNSIGNED NOT NULL,
  `cur_observaciones` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `cur_fecha` date NOT NULL,
  `usu_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tbl_curriculum`
--

INSERT INTO `tbl_curriculum` (`cur_id`, `cur_observaciones`, `cur_fecha`, `usu_id`, `created_at`, `updated_at`) VALUES
(1, 'Curriculum de Susana', '2020-06-13', 1, '2020-06-15 09:53:56', '2020-06-15 09:53:56');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_experiencias`
--

CREATE TABLE `tbl_experiencias` (
  `exp_id` int(10) UNSIGNED NOT NULL,
  `exp_puesto` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `exp_responsabilidades` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `exp_fechaI` date NOT NULL,
  `exp_fechaf` date NOT NULL,
  `exp_empresa` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cur_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tbl_experiencias`
--

INSERT INTO `tbl_experiencias` (`exp_id`, `exp_puesto`, `exp_responsabilidades`, `exp_fechaI`, `exp_fechaf`, `exp_empresa`, `cur_id`, `created_at`, `updated_at`) VALUES
(1, 'Secretaria', 'Contestar el telefono', '2020-06-01', '2020-06-27', 'Matz suplidora', 1, '2020-06-15 09:54:45', '2020-06-15 09:54:45');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_formaciones`
--

CREATE TABLE `tbl_formaciones` (
  `for_id` int(10) UNSIGNED NOT NULL,
  `for_titulo` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `for_especialidad` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `for_institucion` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `for_fecha` date NOT NULL,
  `cur_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tbl_formaciones`
--

INSERT INTO `tbl_formaciones` (`for_id`, `for_titulo`, `for_especialidad`, `for_institucion`, `for_fecha`, `cur_id`, `created_at`, `updated_at`) VALUES
(1, 'Ingles convesacional', 'Idioma ingles', 'Liceo Unesco', '2016-12-20', 1, '2020-06-15 09:56:13', '2020-06-15 09:56:13');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_ofertas`
--

CREATE TABLE `tbl_ofertas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `ofer_categoria` int(10) UNSIGNED NOT NULL,
  `ofer_descripcion` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `ofer_vacantes` int(11) NOT NULL,
  `ofer_fecha` date NOT NULL,
  `ofer_contrato` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `ofer_hora_inicio` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `ofer_hora_fin` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `ofer_dia_inicio` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `ofer_dia_fin` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `ofer_salario` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tbl_ofertas`
--

INSERT INTO `tbl_ofertas` (`id`, `ofer_categoria`, `ofer_descripcion`, `ofer_vacantes`, `ofer_fecha`, `ofer_contrato`, `ofer_hora_inicio`, `ofer_hora_fin`, `ofer_dia_inicio`, `ofer_dia_fin`, `ofer_salario`, `created_at`, `updated_at`) VALUES
(1, 1, 'Asistente administrativo', 2, '2020-06-14', 'Seis meses', '08:00', '15:00', 'Lunes', 'Viernes', 400, '2020-06-15 10:02:23', '2020-06-15 10:02:23'),
(2, 2, 'Soporte tecnico', 1, '2020-06-13', 'Seis meses', '07:00', '17:00', 'Lunes', 'Sabado', 50000, '2020-06-15 10:04:31', '2020-06-15 10:04:31'),
(3, 3, 'Programador en php', 5, '2020-06-13', 'Indefinido', '08:00', '17:00', 'Lunes', 'Sabado', 100000, '2020-06-15 10:10:40', '2020-06-15 10:10:40');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_postulacions`
--

CREATE TABLE `tbl_postulacions` (
  `pos_id` bigint(20) UNSIGNED NOT NULL,
  `pos_usuario` bigint(20) UNSIGNED NOT NULL,
  `pos_oferta` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tbl_postulacions`
--

INSERT INTO `tbl_postulacions` (`pos_id`, `pos_usuario`, `pos_oferta`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2020-06-15 10:12:32', '2020-06-15 10:12:32');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_requisitos`
--

CREATE TABLE `tbl_requisitos` (
  `req_id` int(10) UNSIGNED NOT NULL,
  `req_descripcion` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ofer_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tbl_requisitos`
--

INSERT INTO `tbl_requisitos` (`req_id`, `req_descripcion`, `ofer_id`, `created_at`, `updated_at`) VALUES
(1, 'Bachiller en administracion de empresas', 1, '2020-06-15 10:05:27', '2020-06-15 10:05:27'),
(2, 'Horario flexible', 1, '2020-06-15 10:06:37', '2020-06-15 10:06:37'),
(3, 'Tecnico en soporte', 2, '2020-06-15 10:07:00', '2020-06-15 10:07:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_usuarios`
--

CREATE TABLE `tbl_usuarios` (
  `usu_id` bigint(20) UNSIGNED NOT NULL,
  `usu_nombre` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `usu_apellidos` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `usu_cedula` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `usu_email` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `usu_direccion` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `usu_usuario` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `usu_contra` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `usu_tipo` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `usu_foto` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `usu_telefono` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tbl_usuarios`
--

INSERT INTO `tbl_usuarios` (`usu_id`, `usu_nombre`, `usu_apellidos`, `usu_cedula`, `usu_email`, `usu_direccion`, `usu_usuario`, `usu_contra`, `usu_tipo`, `usu_foto`, `usu_telefono`) VALUES
(1, 'susana', 'cervantes', '117050374', 'susi0326@gmail.com', 'Vila Ligia', 'susana', '123', 'u', 'smile.png', '87873193'),
(2, 'Microsoft', NULL, '11698599', 'susi0326@gmail.com', 'Vila Ligia', 'usuario', '123', 'e', '90-902662_egg-png-mario-bros-items-png.png', '83375843'),
(3, 'UNA', NULL, '2569826', 'susi0326@gmail.com', 'Sinai, Perez Zeledon', 'una', '123', 'e', 'unnamed.png', '27721020');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tbl_categorias`
--
ALTER TABLE `tbl_categorias`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tbl_categorias_cat_usuario_foreign` (`cat_usuario`);

--
-- Indices de la tabla `tbl_curriculum`
--
ALTER TABLE `tbl_curriculum`
  ADD PRIMARY KEY (`cur_id`),
  ADD KEY `tbl_curriculum_usu_id_foreign` (`usu_id`);

--
-- Indices de la tabla `tbl_experiencias`
--
ALTER TABLE `tbl_experiencias`
  ADD PRIMARY KEY (`exp_id`),
  ADD KEY `tbl_experiencias_cur_id_foreign` (`cur_id`);

--
-- Indices de la tabla `tbl_formaciones`
--
ALTER TABLE `tbl_formaciones`
  ADD PRIMARY KEY (`for_id`),
  ADD KEY `tbl_formaciones_cur_id_foreign` (`cur_id`);

--
-- Indices de la tabla `tbl_ofertas`
--
ALTER TABLE `tbl_ofertas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tbl_ofertas_ofer_categoria_foreign` (`ofer_categoria`);

--
-- Indices de la tabla `tbl_postulacions`
--
ALTER TABLE `tbl_postulacions`
  ADD PRIMARY KEY (`pos_id`),
  ADD KEY `tbl_postulacions_pos_usuario_foreign` (`pos_usuario`),
  ADD KEY `tbl_postulacions_pos_oferta_foreign` (`pos_oferta`);

--
-- Indices de la tabla `tbl_requisitos`
--
ALTER TABLE `tbl_requisitos`
  ADD PRIMARY KEY (`req_id`),
  ADD KEY `tbl_requisitos_ofer_id_foreign` (`ofer_id`);

--
-- Indices de la tabla `tbl_usuarios`
--
ALTER TABLE `tbl_usuarios`
  ADD PRIMARY KEY (`usu_id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;

--
-- AUTO_INCREMENT de la tabla `tbl_categorias`
--
ALTER TABLE `tbl_categorias`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `tbl_curriculum`
--
ALTER TABLE `tbl_curriculum`
  MODIFY `cur_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `tbl_experiencias`
--
ALTER TABLE `tbl_experiencias`
  MODIFY `exp_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `tbl_formaciones`
--
ALTER TABLE `tbl_formaciones`
  MODIFY `for_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `tbl_ofertas`
--
ALTER TABLE `tbl_ofertas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `tbl_postulacions`
--
ALTER TABLE `tbl_postulacions`
  MODIFY `pos_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `tbl_requisitos`
--
ALTER TABLE `tbl_requisitos`
  MODIFY `req_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `tbl_usuarios`
--
ALTER TABLE `tbl_usuarios`
  MODIFY `usu_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `tbl_categorias`
--
ALTER TABLE `tbl_categorias`
  ADD CONSTRAINT `tbl_categorias_cat_usuario_foreign` FOREIGN KEY (`cat_usuario`) REFERENCES `tbl_usuarios` (`usu_id`);

--
-- Filtros para la tabla `tbl_curriculum`
--
ALTER TABLE `tbl_curriculum`
  ADD CONSTRAINT `tbl_curriculum_usu_id_foreign` FOREIGN KEY (`usu_id`) REFERENCES `tbl_usuarios` (`usu_id`);

--
-- Filtros para la tabla `tbl_experiencias`
--
ALTER TABLE `tbl_experiencias`
  ADD CONSTRAINT `tbl_experiencias_cur_id_foreign` FOREIGN KEY (`cur_id`) REFERENCES `tbl_curriculum` (`cur_id`);

--
-- Filtros para la tabla `tbl_formaciones`
--
ALTER TABLE `tbl_formaciones`
  ADD CONSTRAINT `tbl_formaciones_cur_id_foreign` FOREIGN KEY (`cur_id`) REFERENCES `tbl_curriculum` (`cur_id`);

--
-- Filtros para la tabla `tbl_ofertas`
--
ALTER TABLE `tbl_ofertas`
  ADD CONSTRAINT `tbl_ofertas_ofer_categoria_foreign` FOREIGN KEY (`ofer_categoria`) REFERENCES `tbl_categorias` (`id`);

--
-- Filtros para la tabla `tbl_postulacions`
--
ALTER TABLE `tbl_postulacions`
  ADD CONSTRAINT `tbl_postulacions_pos_oferta_foreign` FOREIGN KEY (`pos_oferta`) REFERENCES `tbl_ofertas` (`id`),
  ADD CONSTRAINT `tbl_postulacions_pos_usuario_foreign` FOREIGN KEY (`pos_usuario`) REFERENCES `tbl_usuarios` (`usu_id`);

--
-- Filtros para la tabla `tbl_requisitos`
--
ALTER TABLE `tbl_requisitos`
  ADD CONSTRAINT `tbl_requisitos_ofer_id_foreign` FOREIGN KEY (`ofer_id`) REFERENCES `tbl_ofertas` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
