@extends('layout')

@section('content')

<div class="column-sm-8" >
<a href="{{ route('postulaciones.edit', $oferta->id)}}" class="btn btn-dark">Aplicar a Oferta</a>
	<a href="{{route('postulaciones.index')}}" class="btn btn-dark">Listar ofertas Disponibles</a>
	<h2>{{$usuario->usu_nombre}}</h2>

<p><h5>Descripcion:</h5>
	{{$oferta->ofer_descripcion}} 
</p>

<p>
	<h5>Categoria:</h5>
	{{$categorias->cat_nombre}}
<p>

</p>
	<h5>Numero de vacantes</h5>
	{{$oferta->ofer_vacantes}}
</p>

<p>
	<h5>Fecha de oferta</h5>
	{{$oferta->ofer_fecha}}
</p>

<p>
	<h5>Contrato</h5>
	{{$oferta->ofer_contrato}}
</p>

<p>
	<h5>Dias laborados:</h5>
	{{$oferta->ofer_dia_inicio}}</br>
	{{$oferta->ofer_dia_fin}}</br>
	<h5>Horario:</h5>
	Inicio: {{$oferta->ofer_hora_inicio}}</br>
	Salida: {{$oferta->ofer_hora_fin}}</br>
</p>
</div>


@endsection