@extends('layout')

@section('content')
		<div class="row">
			<div class="col-sm-12">
				<h1 class="text-center page-header">Ofertas laborales Aplicadas</h1>
				<a href="{{ route('postulaciones.index')}}" class="btn btn-outline-dark">Listar disponibles</a>
			</div>
            <br>
		</div>
		<div>
		@include('postulaciones.fragment.error')
		@include('postulaciones.fragment.info')
		</div>
        <br>
		<table class="table table-hover table-dark">
		<thead>
		<tr>	
				<th>Vacantes</th>
				<th>Fecha</th>
				<th>Descripcion</th>
				<th>Empresa</th>
				<th collapse="3">&nbsp;</th>
		</tr>		
		</thead>
		<tbody>
		@foreach( $ofertas as $oferta)
		<tr>
			<td> {{ $oferta->ofer_vacantes}}</td>
			<td>{{ $oferta->ofer_fecha }} </td>
			<td> {{ $oferta->ofer_descripcion}}</td>
			<td> {{ $oferta->usu_nombre}}</td>
			<td><form action="{{ route('postulaciones.destroy', $oferta->id) }}" method="post"> 
					{{csrf_field()}}
					<input type="hidden" name="_method" value="DELETE">
					<button class="btn btn-light"> Eliminar</button>
				</form> </td>
		</tr>
		@endforeach
		</tbody>
	</table>
@endsection