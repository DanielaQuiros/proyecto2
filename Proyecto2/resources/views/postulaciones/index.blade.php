@extends('layout')

@section('content')
		<div class="row">
			<div class="col-sm-12">
				<h1 class="text-center page-header">Ofertas laborales disponibles</h1>
				<!--<a href="{{ route('ofertas.create')}}" class="btn btn-primary">Nueva</a>-->
				<a href="{{ route('postulaciones.aplicadas')}}" class="btn btn-outline-dark">Aplicadas</a>
                <br>
				<form action="{{ route('postulaciones.filtrar') }}" method="GET"> 
                    <div style="display: -webkit-box;">
                        <input type="text" class="form-control" name="valor" placeholder="Empresa o categoria">
                        <button class="btn btn-info"> BUSCAR</button>
                    </div>
				</form>
			</div>
		</div>

		<div>
		@include('postulaciones.fragment.info')
		</div>
		<table class="table table-hover table-dark">
		<thead>
		<tr>	
				<th>Vacantes</th>
				<th>Fecha</th>
				<th>Descripcion</th>
				<th>Empresa</th>
				<th collapse="3">&nbsp;</th>
		</tr>		
		</thead>
		<tbody>
		@foreach( $ofertas as $oferta)
		<tr>
			<td> {{ $oferta->ofer_vacantes}}</td>
			<td>{{ $oferta->ofer_fecha }} </td>
			<td> {{ $oferta->ofer_descripcion}}</td>
			<td> {{ $oferta->usu_nombre}}</td>
			
			<td><a href="{{ route('postulaciones.show', $oferta->id)}}" class="btn btn-light">Ver</a></td>
			<td><a href="{{ route('postulaciones.edit', $oferta->id)}}" class="btn btn-light">Aplicar a Oferta</a></td>
		</tr>
		@endforeach
		</tbody>
	</table>
@endsection