@extends('layout')

@section('content')
		<div class="row">
			<div class="col-sm-12">
				<h1 class="text-center page-header">Candidatos</h1>
				<a href="{{route('ofertas.index')}}" class="btn btn-outline-dark">Listar ofertas</a>
                <br>
			</div>
		</div>

		<div>
		@include('postulaciones.fragment.info')
		</div>
		<table class="table table-hover table-dark">
		<thead>
		<tr>	
				<th>Nombre</th>
				<th>Apellido</th>
				<th>Correo</th>
				<th collapse="3">&nbsp;</th>
		</tr>		
		</thead>
		<tbody>
		@foreach( $Usuarios as $usu)
		<tr>
			<td> {{ $usu->usu_nombre}}</td>
			<td> {{ $usu->usu_apellidos}} </td>
			<td> {{ $usu->usu_email}}</td>
			<td><a href="{{route('curriculum.show', $usu->usu_id)}}" class="btn btn-light">Curriculum</a></td>
			<td><a href="{{route('usuario.show', $usu->usu_id)}}" class="btn btn-light">Información</a></td>
		</tr>
		@endforeach
		</tbody>
	</table>
@endsection