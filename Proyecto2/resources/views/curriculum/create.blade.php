@extends('layout')

@section('content')

<div class="column-sm-8">
	<h2>Crear Curriculum <a href="{{route('curriculum.index')}}" class="btn btn-default">Listar Curriculum</a></h2>
	@include('curriculum.fragment.error')

	{!!Form::open(['route' => 'curriculum.store'])!!}
	@include('curriculum.fragment.form')
	{!!Form::close() !!}
	</div>
@endsection
