@extends('layout')


@section('content')
	<div class="col-sm-8">
		<h2>Curriculum
			<a href="{{ route('curriculum.create')}}" class="btn btn-dark pull-right">Nuevo</a>
			<a href="{{ route('curriculum.show', Session::get('usuarioActual')->usu_id)}}" class="btn btn-outline-dark pull-right">Ver</a>
		</h2>
		<br>
		<div>
			@if($cur != null)
				<label><strong>Observaciones</strong></label>
				<br>	
				<label>{{ $cur->cur_observaciones }}</label>
				<br>
				<label><strong>Fecha</strong></label>
				<br>
				<label>{{ $cur->cur_fecha  }}</label>
				<br>

				<h2>Experiencias laborales
					<a href="{{route('experiencia.create')}}" class="btn btn btn-info">Agregar</a>
				</h2>
				<table class="table table-dark" style="width: 150%;">
					<tr> <th>Puesto</th><th>Responsabilidades</th><th>Fecha De Inicio</th><th>Fecha Final</th><th>Empresa</th></tr>
					<tbody>
						@foreach( $exp as $e)
						<tr>
							<td>{{ $e->exp_puesto }}</td>
						    <td>{{ $e->exp_responsabilidades }}</td>
						    <td>{{ $e->exp_fechaI }} </td>
						    <td>{{ $e->exp_fechaf }}</td>
						    <td>{{ $e->exp_empresa }}</td>
						    <td><a href="{{ route('experiencia.edit', $e->exp_id)}}" class="btn btn-light">Editar</a></td>
						    <td>
								<form action="{{ route('experiencia.destroy', $e->exp_id) }}" method="post"> 
									{{csrf_field()}}
									<input type="hidden" name="_method" value="DELETE">
									<button class="btn btn-light"> Eliminar</button>
								</form>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
				<br>
				<h2>Formaciones academicas
					<a href="{{route('formacion.create')}}" class="btn btn btn-info">Agregar</a>
				</h2>
				<table class="table table-dark" style="width: 150%;">
					<tr> <th>Titulo</th><th>Especialidad</th><th>Institucion</th><th>Fecha</th></tr>
					<tbody>
						@foreach( $for as $f)
						<tr>
							<td>{{ $f->for_titulo }}</td>
						    <td>{{ $f->for_especialidad }}</td>
						    <td>{{ $f->for_institucion }} </td>
						    <td>{{ $f->for_fecha }}</td>
						    <td><a href="{{ route('formacion.edit', $f->for_id)}}" class="btn btn-light">Editar</a></td>
						    <td>
								<form action="{{ route('formacion.destroy', $f->for_id) }}" method="post"> 
									{{csrf_field()}}
									<input type="hidden" name="_method" value="DELETE">
									<button class="btn btn-light"> Eliminar</button>
								</form>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
		</div>
		@endif
	</div>
@endsection
