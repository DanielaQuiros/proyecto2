@extends('layout')

@section('content')

<!DOCTYPE html">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Resume</title>
<link type="text/css" rel="stylesheet" href="css/blue.css" />
<link type="text/css" rel="stylesheet" href="css/print.css" media="print"/>
</head>
<body>
<!-- Begin Wrapper -->
<div style="background-color: white;">
<div class="container">
    <div>
        <div style="position: relative; text-align: right; padding-top: 60px;">
            <img style="width: 15%;" src="{{ asset('/photos/logo.png')}}">
            <div id='fecha' style="margin-right: 5px;"></div>
        </div>
        <h1 style="position: relative; text-align: left;">Curriculum</h1>
    </div>
</div>
<div id="wrapper">
  <div class="wrapper-top"></div>
  <div class="wrapper-mid">
    <!-- Begin Paper -->
    <div id="paper">
      <div id="paper-mid">
        <div class="entry">
          <!-- Begin Image -->
          <img style="width: 10%; height: 10%;" class="portrait" src="{{ asset('/photos/'. $usu->usu_foto) }}"/>
          <div class="self">
            <h1 class="name">{{ $usu->usu_nombre . ' '. $usu->usu_apellidos }} <br />
              <span>{{$usu->usu_cedula}}</span></h1>
            <ul>
              <li class="ad">{{$usu->usu_direccion}}</li>
              <li class="mail">{{$usu->usu_email}}</li>
              <li class="tel">{{$usu->usu_telefono}}</li>
            </ul>
          </div>
          <!-- End Personal Information -->
        </div>
        <!-- Begin 1st Row -->
        <div class="entry">
          <h2>Observaciones</h2>
          <p>{{$cur->cur_observaciones}}</p>
        </div>
	<div class="social">
            <ul>
              <li>{{$cur->cur_fecha}}</li>
              </ul>
        </div>
        <!-- End 1st Row -->
        <!-- Begin 2nd Row -->
        <div class="entry">
            <br>
          <h2>Formaciones academicas</h2>
          <div class="content">
            <table class="table">
			<tr> <th>Titulo</th><th>Especialidad</th><th>Institucion</th><th>Fecha</th></tr>
			<tbody>
				@foreach( $for as $f)
				<tr>
					<td>{{ $f->for_titulo }}</td>
				    <td>{{ $f->for_especialidad }}</td>
				    <td>{{ $f->for_institucion }} </td>
				    <td>{{ $f->for_fecha }}</td>
				</tr>
				@endforeach
			</tbody>
		</table>
          </div>
        </div>
        <!-- End 2nd Row -->
        <!-- Begin 3rd Row -->
        <div class="entry">
            <br>
          <h2>Experiencias laborales</h2>
          <div class="content">
            <table class="table">
			<tr> <th>Puesto</th><th>Responsabilidades</th><th>Fecha De Inicio</th><th>Fecha Final</th><th>Empresa</th></tr>
			<tbody>
				@foreach( $exp as $e)
				<tr>
					<td>{{ $e->exp_puesto }}</td>
				    <td>{{ $e->exp_responsabilidades }}</td>
				    <td>{{ $e->exp_fechaI }} </td>
				    <td>{{ $e->exp_fechaf }}</td>
				    <td>{{ $e->exp_empresa }}</td>
				</tr>
				@endforeach
			</tbody>
		</table>
          </div>
      </div>
      <div class="clear"></div>
    </div>
  </div>
</div>
</div>
</div>
    <script>
    var d = new Date(),
    month = '' + (d.getMonth() + 1),
    day = '' + d.getDate(),
    year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    d = [year, month, day].join('-');
    document.getElementById('fecha').innerHTML = '<h2>'+ d +'</h2>';
    window.print();
</script>
</body>
</html>

@endsection