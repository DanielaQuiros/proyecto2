@extends('layout')

@section('content')

<div class="column-sm-12">
	<h2>Editar Curriculum 
	<a href="{{route('curriculum.index')}}" class="btn btn-default">Listar Curriculum</a></h2>
	@include('curriculum.fragment.error')
	
	{!!Form::model($curriculum, ['route' => ['curriculum.update',$curriculum->cur_id], 'method' => 'PUT'])!!}
	@include('curriculum.fragment.form')
	{!!Form::close() !!}

	</div>

@endsection
