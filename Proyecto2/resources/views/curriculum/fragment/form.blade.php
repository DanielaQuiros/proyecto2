<div class="form-group">
	{!! Form::label('cur_observaciones','Observaciones del curriculum') !!}
	{!! Form::text('cur_observaciones',null,['class'=>'form-control']) !!}
</div>

<div class="form-group">
	{!! Form::label('cur_fecha','Fecha del curriculum') !!}
	{!! Form::date('cur_fecha',null,['class'=>'form-control']) !!}
</div>

<div class="form-group">
	{!! Form::hidden('usu_id',Session::get('usuarioActual')->usu_id)!!}
</div>

<div class="form-group">
	{!! Form::submit('Guardar',['class'=>'btn btn-dark']) !!}
	
</div>