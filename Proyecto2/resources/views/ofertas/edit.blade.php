@extends('layout')

@section('content')

<div class="column-sm-12">
	<h2>Editar ofertas 
	<a href="{{route('ofertas.index')}}" class="btn btn-default">Listar ofertas</a></h2>

	@include('ofertas.fragment.error')
	
	{!!Form::model($oferta, ['route' => ['ofertas.update',$oferta->id], 'method' => 'PUT'])!!}
	@include('ofertas.fragment.form')
	{!!Form::close() !!}

	</div>

	</script>
@endsection
