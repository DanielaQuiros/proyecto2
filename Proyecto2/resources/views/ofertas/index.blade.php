@if(Session::get('usuarioActual')->usu_tipo != 'e')
	<h5>Permisos NO validos</h5>
@endif
@if(Session::get('usuarioActual')->usu_tipo == 'e')
@extends('layout')
@section('content')
		<div class="row">
			<div class="col-sm-12">
				<h1 class="text-center page-header">Ofertas laborales</h1>
				<a href="{{ route('ofertas.create')}}" class="btn btn-outline-dark" style="margin-botton:15px;">Nueva</a>
			</div>
		</div>
		<div>
			
		@include('ofertas.fragment.info')
		</div>
		<table class="table table-hover table-dark">
		<thead>
		<tr>	
				<th>Vacantes</th>
				<th>Fecha</th>
				<th>Descripcion</th>
			
				<th collapse="3">&nbsp;</th>
		</tr>		
		</thead>
		<tbody>
		@foreach( $ofertas as $oferta)
		<tr>
			<td> {{ $oferta->ofer_vacantes}}</td>
			<td>{{ $oferta->ofer_fecha }} </td>
			<td> {{ $oferta->ofer_descripcion}}</td>
			<td><a href="{{ route('ofertas.show', $oferta->id)}}" class="btn btn-light">Ver</a></td>
			<td><a href="{{ route( 'ofertas.edit', $oferta->id)}}" class="btn btn-light">Editar</a></td>
			<td><a href="{{ route('requisito.index',['id'=>$oferta->id])}}" class="btn btn-light">Requisitos</a></td>
			<td>
				<form action="{{ route('ofertas.destroy', $oferta->id) }}" method="post"> 
					{{csrf_field()}}
					<input type="hidden" name="_method" value="DELETE">
					<button class="btn btn-light"> Eliminar</button>
				</form>
			</td>
		</tr>
		@endforeach
		</tbody>
	</table>

@endsection
@endif
