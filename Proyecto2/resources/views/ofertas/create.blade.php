@extends('layout')

@section('content')

<div class="column-sm-8">
	<h2>Crear ofertas <a href="{{route('ofertas.index')}}" class="btn btn-dark">Listar ofertas</a></h2>
	@include('ofertas.fragment.error')

	{!!Form::open(['route' => 'ofertas.store'])!!}
	@include('ofertas.fragment.form')
	{!!Form::close() !!}
	</div>
@endsection
