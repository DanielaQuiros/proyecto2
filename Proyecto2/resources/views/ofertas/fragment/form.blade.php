<div class="form-group">
	{!! Form::label('ofer_descripcion','Descripcion de la Oferta') !!}
	{!! Form::textarea('ofer_descripcion',null,['class'=>'form-control']) !!}
</div>

<div class="form-group">
	{!! Form::label('ofer_categoria','Categoria') !!}
	{!! Form::select('ofer_categoria', $categorias)
	!!}
	<!-- Form::select('ofer_categoria', ['1' => 'Uno', '2' => 'Dos']) -->
</div>

<div class="form-group">
	{!! Form::label('ofer_vacantes','Numero de vacantes de la Oferta') !!}
	{!! Form::text('ofer_vacantes',null,['class'=>'form-control']) !!}
</div>

<div class="form-group">
	{!! Form::label('ofer_fecha','Fecha de la Oferta') !!}
	{!! Form::date('ofer_fecha',null,['class'=>'form-control']) !!}
</div>

<div class="form-group">
	{!! Form::label('ofer_contrato','Contrato de la Oferta') !!}
	{!! Form::text('ofer_contrato',null,['class'=>'form-control']) !!}
</div>

<div class="form-group">
	{!! Form::label('ofer_salario','Salario de la Oferta') !!}
	{!! Form::number('ofer_salario',null,['class'=>'form-control']) !!}
</div>

<div class="form-group">
	{!! Form::label('ofer_hora_inicio','Hora de Inicio') !!}
	{!! Form::time('ofer_hora_inicio',null,['class'=>'form-control']) !!}
</div>

<div class="form-group">
	{!! Form::label('ofer_hora_fin','Hora de Salida') !!}
	{!! Form::time('ofer_hora_fin',null,['class'=>'form-control']) !!}
</div>
<div class="form-group">
	{!! Form::label('ofer_dia_inicio','Dia de Inicio') !!}
	{!! Form::text('ofer_dia_inicio',null,['class'=>'form-control']) !!}
</div>
<div class="form-group">
	{!! Form::label('ofer_dia_fin','Dia final') !!}
	{!! Form::text('ofer_dia_fin',null,['class'=>'form-control']) !!}
</div>

<div class="form-group">
	{!! Form::submit('Guardar',['class'=>'btn btn-dark']) !!}
	
</div>