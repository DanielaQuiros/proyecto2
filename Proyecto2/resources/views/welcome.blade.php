<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>SIRHENA</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            
        @if(count($errors) > 0)
        
        <div class="alert alert-danger">
            <ul>
            @foreach($errors->all() as $error)
                <li>{{$error}}</li>
            @endforeach
            </ul>
        </div>
        @endif
        
        @if(\Session::has('success'))
            <div class="alert alert-success" role="alert">
                <p> {{\Session::get('success')}} </p>
            </div>
        @endif
    </div>
        
    <div style="display:left; background-color: aliceblue;" >
        <a style="color: black;" href="{{route('usuario.logout')}}"><strong>Cerrar sesión</strong></a>
        @if(session()->has('user_id')) 
            
        @endif
    </div>
        
        <div class="flex-center position-ref full-height" style="background-color: #423e3edd;">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md" style="color: aliceblue;">
                    SIRHENA
                </div>

                <div class="links">
                    <ul class="list-group">
                    <li style="background-color: rgb(41, 41, 41); color: white;" class="list-group-item"><a data-toggle="modal" data-target="#editarUsuario">Modificar usuario</a></li>
                    
                     @if(Session::get('usuarioActual')->usu_tipo != 'e')
                        <li style="background-color: rgb(41, 41, 41); color: white;" class="list-group-item"><a style="color: white;" href="{{route('curriculum.index')}}">Curriculum</a></li>
                        <li style="background-color: rgb(41, 41, 41); color: white;" class="list-group-item"><a style="color: white;" href="{{route('postulaciones.index')}}">Ofertas Disponibles</a></li>
                        <li style="background-color: rgb(41, 41, 41); color: white;" class="list-group-item"><a style="color: white;" href="{{route('reporte.ofertasAplicadas')}}">Ofertas inscritas</a></li>
                    @endif
                    @if(Session::get('usuarioActual')->usu_tipo == 'e')
                        <li style="background-color: rgb(41, 41, 41); color: white;" class="list-group-item"><a style="color: white;" href="{{route('categoria.index')}}">Categorías</a></li>
                        <li style="background-color: rgb(41, 41, 41); color: white;" class="list-group-item"><a style="color: white;" href="{{route('ofertas.index')}}">Ofertas</a></li>
                     @endif
                        
                    <li style="background-color: rgb(41, 41, 41); color: white;" class="list-group-item"><a style="color: white;" href="{{route('reporte.empresa')}}">Consultar empresa</a></li>
                    <li style="background-color: rgb(41, 41, 41); color: white;" class="list-group-item"><a style="color: white;" href="{{route('grafico.index')}}">Vacantes por empresa</a></li>
                    <li style="background-color: rgb(41, 41, 41); color: white;" class="list-group-item"><a style="color: white;" href="{{route('reporte.vacantesCategoria')}}">Vacantes por categoría</a></li>
                    </ul>
                </div>
            </div>
        </div>
        
        <div class="modal fade" id="editarUsuario" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Modificar usuario</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
        <form action="{{ route('usuario.update', Session::get('usuarioActual')->usu_id) }}" method="POST" enctype="multipart/form-data">
            {{ method_field('PUT') }}{{csrf_field()}}
            <div class="modal-body">
                
              <div class="form-group">
                <label>Nombre</label>
                <input type="text" class="form-control" name="usu_nombre" id="usu_nombre" value="{{ Session::get('usuarioActual')->usu_nombre }}">
              </div>
                
              <div class="form-group">
                <label>Apellidos</label>
                <input type="text" class="form-control" name="usu_apellidos" id="usu_apellidos" value="{{ Session::get('usuarioActual')->usu_apellidos }}">
              </div>
                
              <div class="form-group">
                <label>Cédula/Cédula Jurídica</label>
                <input type="text" class="form-control" name="usu_cedula" id="usu_cedula" value="{{ Session::get('usuarioActual')->usu_cedula }}">
              </div>
                
              <div class="form-group">
                <label>Correo</label>
                <input type="email" class="form-control" name="usu_email" id="usu_email" value="{{ Session::get('usuarioActual')->usu_email }}">
              </div>
                
              <div class="form-group">
                <label>Teléfono</label>
                <input type="text" class="form-control" name="usu_telefono" id="usu_telefono" value="{{ Session::get('usuarioActual')->usu_telefono }}">
              </div>
                
              <div class="form-group">
                <label>Dirección</label>
                <textarea class="form-control" name="usu_direccion" id="usu_direccion" rows="3">{{Session::get('usuarioActual')->usu_direccion}}</textarea>
              </div>
                
             <div class="form-group">
                <label>Nombre de Usuario</label>
                <input type="text" class="form-control" name="usu_usuario" id="usu_usuario" value="{{ Session::get('usuarioActual')->usu_usuario }}">
              </div>
                
              <div class="form-group">
                <label>Contraseña</label>
                <input type="password" class="form-control" name="contrasena" id="contrasena" value="{{ Session::get('usuarioActual')->usu_contra}}">
              </div>
                
              <div class="form-group">
                  <label>Tipo de usuario:</label>
                  <select name="usu_tipo" class="form-control" title="Tipo de usuario">
                      @if (Session::get('usuarioActual')->usu_tipo == 'u')
                            <option value="u" selected="selected">Candidato</option>
                            <option value="e">Empresa</option>
                      @endif
                      @if (Session::get('usuarioActual')->usu_tipo == 'e')
                            <option value="u">Candidato</option>
                            <option value="e" selected="selected">Empresa</option>
                      @endif
                  </select>
              </div>
                
             <div>
                <img style="width: 40%; height: 40%;" src="{{ asset('/photos/'.Session::get('usuarioActual')->usu_foto) }}">
            </div>
                
              <div class="form-group">
                <label>Foto</label>
                <input type="file" class="form-control-file" name="usu_foto" id="usu_foto" accept="image/jpeg,image/gif,image/png">
              </div>
                
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            <button type="submit" class="btn btn-primary">Guardar</button>
          </div>
        </form>
        </div>
      </div>
    </div>
    
    
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
        
        
    </body>
</html>
