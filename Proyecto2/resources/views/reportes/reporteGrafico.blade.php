<!DOCTYPE html>
<html lang="en">

    <body>
    <br>
    <div class="container">
        <div>
            <div style="position: relative; text-align: right; padding-top: 60px;">
                <img style="width: 15%;" src="{{ asset('/photos/logo.png')}}">
                <div id='fecha' style="margin-right: 5px;"></div>
            </div>
            <h1 style="position: relative; text-align: left;">Cantidad de empleos vacantes por empresa</h1>
        </div>
        <ul class="list-group list-group-flush">
            @foreach($datos as $result)
                <li class="list-group-item">{{ $result->usu_nombre . ': ' . $result->total }}</li>
            @endforeach
        </ul>
        <br>
        <div class="row" style="width: 225%;">
            <div class="col-6">
                <div class="card rounded">
                    <div class="card-body py-3 px-3">
                        {!! $usersChart->container() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
        
    </body>
    
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js" charset="utf-8">
        </script>
            {{-- ChartScript --}}
            @if($usersChart)
                {!! $usersChart->script() !!}
            @endif
    </head>

<script src="/js/vendor/chart.min.js"></script>
<script>
    var d = new Date(),
    month = '' + (d.getMonth() + 1),
    day = '' + d.getDate(),
    year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    d = [year, month, day].join('-');
    document.getElementById('fecha').innerHTML = '<h2>'+ d +'</h2>';
    window.print();
</script>
</html>