@extends('layout')

@section('content')
<div class="form-group" >
    <label>Nombre de la empresa:</label>
    <form action="{{ route('reporte.buscarEmpresa') }}" method="GET">
        {{ method_field('GET') }}{{csrf_field()}}
        <div style="display: flex;">
            <input type="text" class="form-control" name="nombre">
            <button type="submit" class="btn btn-dark" style="color:white;">Buscar</button>
        </div>
    </form>
</div>
<table class="table table-dark">
  <thead>
    <tr>
      <th scope="col">Nombre</th>
      <th scope="col"> </th>
    </tr>
  </thead>
  <tbody>
    @foreach($empresas as $empresa)
    <tr>
      <td>{{ $empresa->usu_nombre }}</td>
      <td><a class="btn btn-light" href="{{route('reporte.mostrarEmpresa', $empresa->usu_id)}}">Ver</a></td>
    </tr>
    @endforeach
  </tbody>
</table>


@endsection