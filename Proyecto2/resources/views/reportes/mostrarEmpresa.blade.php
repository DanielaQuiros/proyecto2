@extends('layout')

@section('content')
<div style="background-color:white;">
<div>
    <div style="position: relative; text-align: right; padding-top: 60px;">
        <img style="width: 15%;" src="{{ asset('/photos/logo.png')}}">
        <div id='fecha' style="margin-right: 5px;"></div>
    </div>
    <h1 style="position: relative; text-align: left;">Información de la empresa</h1>
</div>

<div class="section" id="about">
  <div class="container">
    <div class="card" data-aos="fade-up" data-aos-offset="10">
      <div class="row">
        <div class="col-lg-6 col-md-12">
          <div class="card-body">
            <div class="h4 mt-0 title"></div>
            <img style="width: 25%; height: 25%;" src="{{ asset('/photos/'.$usu->usu_foto) }}">
          </div>
        </div>
        <div class="col-lg-6 col-md-12">
          <div class="card-body">
            <div class="h4 mt-0 title">Información básica</div>
            <div class="row">
              <div class="col-sm-4"><strong class="text-uppercase">Nombre:</strong></div>
              <div class="col-sm-8">{{$usu->usu_nombre }}</div>
            </div>
            <div class="row mt-3">
              <div class="col-sm-4"><strong class="text-uppercase">Correo:</strong></div>
              <div class="col-sm-8">{{$usu->usu_email}}</div>
            </div>
            <div class="row mt-3">
              <div class="col-sm-4"><strong class="text-uppercase">Teléfono:</strong></div>
              <div class="col-sm-8">{{$usu->usu_telefono}}</div>
            </div>
            <div class="row mt-3">
              <div class="col-sm-4"><strong class="text-uppercase">Dirección:</strong></div>
              <div class="col-sm-8">{{$usu->usu_direccion}}</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<script>
    var d = new Date(),
    month = '' + (d.getMonth() + 1),
    day = '' + d.getDate(),
    year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    d = [year, month, day].join('-');
    document.getElementById('fecha').innerHTML = '<h2>'+ d +'</h2>';
    window.print();
</script>

@endsection