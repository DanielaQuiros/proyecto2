
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>

<div class="container">
    <div style="position: relative; text-align: right; padding-top: 60px;">
        <img style="width: 15%;" src="{{ asset('/photos/logo.png')}}">
        <div id='fecha' style="margin-right: 5px;"></div>
    </div>
    <h1 style="position: relative; text-align: left;">Cantidad de empleos vacantes por categoría</h1>
    <br>
    <ul class="list-group list-group-flush">
        @foreach($datos as $result)
            <li class="list-group-item"><h3>{{ $result->cat_nombre . ': ' . $result->total }}</h3></li>
        @endforeach
    </ul>
    
</div>


<script>
    var d = new Date(),
    month = '' + (d.getMonth() + 1),
    day = '' + d.getDate(),
    year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    d = [year, month, day].join('-');
    document.getElementById('fecha').innerHTML = '<h2>'+ d +'</h2>';
    window.print();
</script>
