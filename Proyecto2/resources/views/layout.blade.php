<!DOCTYPE html>
<html>
<head>
	<title>Proyecto2</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    
</head>
<body style="background-color: aliceblue;">
	<div class="container"> 
		<div class="row">
			<div class="col-sm-12">
				<a href="{{route('categoria.inicio')}}" class="btn btn-outline-dark float-right">Inicio</a>
			</div>

			@yield('content') 
		</div>
	</div>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
<!--<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>-->
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>

</body>
</html>
