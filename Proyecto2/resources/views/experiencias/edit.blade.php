@extends('layout')

@section('content')

<div class="column-sm-12">
	<h2>Editar Experiencia</h2>
	@include('experiencias.fragment.error')
	
	{!!Form::model($experiencia, ['route' => ['experiencia.update',$experiencia->exp_id], 'method' => 'PUT'])!!}
	@include('experiencias.fragment.form')
	{!!Form::close() !!}

	</div>

@endsection
