@extends('layout')

@section('content')

<div class="column-sm-8">
	<h2>Crear experiencia laboral  <a href="{{route('curriculum.index')}}" class="btn btn-info">Volver</a></h2>
	@include('experiencias.fragment.error')

	{!!Form::open(['route' => 'experiencia.store'])!!}
	@include('experiencias.fragment.form')
	{!!Form::close() !!}
	</div>
@endsection
