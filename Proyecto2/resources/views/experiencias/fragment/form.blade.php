<div class="form-group">
	{!! Form::label('exp_puesto','Puesto de la experiencia') !!}
	{!! Form::text('exp_puesto',null,['class'=>'form-control']) !!}
</div>

<div class="form-group">
	{!! Form::label('exp_responsabilidades','Responsabilidades experiencia') !!}
	{!! Form::text('exp_responsabilidades',null,['class'=>'form-control']) !!}
</div>

<div class="form-group">
	{!! Form::label('exp_fechaI','Fecha de inicio de la experiencia') !!}
	{!! Form::date('exp_fechaI',null,['class'=>'form-control']) !!}
</div>

<div class="form-group">
	{!! Form::label('exp_fechaf','Fecha final de la experiencia') !!}
	{!! Form::date('exp_fechaf',null,['class'=>'form-control']) !!}
</div>

<div class="form-group">
	{!! Form::label('exp_empresa','Empresa de la experiencia') !!}
	{!! Form::text('exp_empresa',null,['class'=>'form-control']) !!}
</div>

<div class="form-group">
	{!! Form::hidden('cur_id',$cur) !!}
</div>

<div class="form-group">
	{!! Form::submit('Guardar',['class'=>'btn btn-dark']) !!}
	
</div>