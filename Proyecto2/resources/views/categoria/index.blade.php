@if(Session::get('usuarioActual')->usu_tipo != 'e')
	<h5>Permisos NO validos</h5>
@endif
@if(Session::get('usuarioActual')->usu_tipo == 'e')@extends('layout')

@section('content')
		<div class="row">
			<div class="col-sm-12">
				<h1 class="text-center page-header">Categorias </h1>
				<a href="{{ route('categoria.create')}}" class="btn btn-outline-dark">Nueva</a>
			</div>
		</div>
		<div>
			
		@include('categoria.fragment.info')
		</div>
		<table class="table table-hover table-dark">
		<thead>
		<tr>	
				<th>Nombre</th>
				<th>Descripcion</th>			
				<th collapse="3">&nbsp;</th>
		</tr>		
		</thead>
		<tbody>
		@foreach( $Categorias as $categ)
		<tr>
			<td> {{ $categ->cat_nombre}}</td>
			<td> {{ $categ->cat_description}}</td>
			<td><a href="{{ route('categoria.show', $categ->id)}}" class="btn btn-light">Ver</a></td>
			<td><a href="{{ route('categoria.edit', $categ->id)}}" class="btn btn-light">Editar</a></td>

			<td>
				<form action="{{ route('categoria.destroy', $categ->id) }}" method="post"> 
					{{csrf_field()}}
					<input type="hidden" name="_method" value="DELETE">
					<button class="btn btn-light"> Eliminar</button>
				</form>
			</td>
		</tr>
		@endforeach
		</tbody>
	</table>

@endsection

@endif