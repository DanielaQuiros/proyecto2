@extends('layout')

@section('content')

<div class="column-sm-8">
	<h2>Crear Categoria <a href="{{route('categoria.index')}}" class="btn btn-outline-dark">Listar categorias</a></h2>
	@include('categoria.fragment.error')

	{!!Form::open( ['route' => 'categoria.store'])!!}
	@include('categoria.fragment.form')
	{!!Form::close() !!}
	</div>
@endsection
