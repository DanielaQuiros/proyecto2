<div class="form-group">
	{!! Form::label('cat_nombre','Nombre de la categoria') !!}
	{!! Form::text('cat_nombre',null,['class'=>'form-control']) !!}
</div>

<div class="form-group">
	{!! Form::label('cat_description','Descripcion') !!}
	{!! Form::textarea('cat_description',null,['class'=>'form-control']) !!}
	{!! Form::hidden('cat_usu',Session::get('usuarioActual')->usu_id,['class'=>'form-control']) !!}
</div>

<div class="form-group">
	{!! Form::submit('Guardar',['class'=>'btn btn-secondary']) !!}
	
</div>