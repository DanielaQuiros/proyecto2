@extends('layout')

@section('content')

<div class="column-sm-8">
<a href="{{ route('categoria.edit', $categoria->id)}}" class="btn btn-dark">Editar</a>
	<a href="{{route('categoria.index')}}" class="btn btn-dark">Listar categoria</a>
<h2>{{$categoria->cat_nombre}} </h2>

<p>
	<h5>Descripcion</h5>
	{{$categoria->cat_description}}
</p>
</div>

@endsection 