@extends('layout')

@section('content')

<div class="column-sm-12">
	<h2>Editar categoria 
	<a href="{{route('categoria.index')}}" class="btn btn-default">Listar categoria</a></h2>
	@include('categoria.fragment.error')
	
	{!!Form::model($categoria, ['route' => ['categoria.update',$categoria->id], 'method' => 'PUT'])!!}
	@include('categoria.fragment.form')
	{!!Form::close() !!}

	</div>

@endsection
