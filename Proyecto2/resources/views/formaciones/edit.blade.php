@extends('layout')

@section('content')

<div class="column-sm-12">
	<h2>Editar Formacion</h2>
	@include('experiencias.fragment.error')
	
	{!!Form::model($formacion, ['route' => ['formacion.update',$formacion->for_id], 'method' => 'PUT'])!!}
	@include('formaciones.fragment.form')
	{!!Form::close() !!}

	</div>

@endsection