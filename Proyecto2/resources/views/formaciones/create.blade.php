@extends('layout')

@section('content')

<div class="column-sm-8">
	<h2>Crear Formacion academica<a href="{{route('curriculum.index')}}" class="btn btn-dark">Volver</a></h2>
	@include('experiencias.fragment.error')

	{!!Form::open(['route' => 'formacion.store'])!!}
	@include('formaciones.fragment.form')
	{!!Form::close() !!}
	</div>
@endsection