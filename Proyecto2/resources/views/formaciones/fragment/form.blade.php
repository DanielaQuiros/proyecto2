<div class="form-group">
	{!! Form::label('for_titulo','Titulo de la formacion') !!}
	{!! Form::text('for_titulo',null,['class'=>'form-control']) !!}
</div>

<div class="form-group">
	{!! Form::label('for_especialidad','Especialidad de la formacion') !!}
	{!! Form::text('for_especialidad',null,['class'=>'form-control']) !!}
</div>

<div class="form-group">
	{!! Form::label('for_institucion','Institucion de la formacion') !!}
	{!! Form::text('for_institucion',null,['class'=>'form-control']) !!}
</div>

<div class="form-group">
	{!! Form::label('for_fecha','Fecha de la formacion') !!}
	{!! Form::date('for_fecha',null,['class'=>'form-control']) !!}
</div>

<div class="form-group">
	{!! Form::hidden('cur_id',$cur,['class'=>'form-control'])!!}
</div>


<div class="form-group">
	{!! Form::submit('Guardar',['class'=>'btn btn-dark']) !!}
	
</div>