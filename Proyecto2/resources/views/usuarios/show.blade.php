@extends('layout')

@section('content')
<div class="section" id="about">
  <div class="container">
    <div class="card" data-aos="fade-up" data-aos-offset="10">
      <div class="row">
        <div class="col-lg-6 col-md-12">
          <div class="card-body">
            <div class="h4 mt-0 title">Información personal</div>
            <img style="width: 25%; height: 25%;" src="{{ asset('/photos/'.$usu->usu_foto) }}">
          </div>
        </div>
        <div class="col-lg-6 col-md-12">
          <div class="card-body">
            <div class="h4 mt-0 title">Contacto</div>
            <div class="row">
              <div class="col-sm-4"><strong class="text-uppercase">Nombre:</strong></div>
              <div class="col-sm-8">{{$usu->usu_nombre }} {{$usu->usu_apellidos}}</div>
            </div>
            <div class="row mt-3">
              <div class="col-sm-4"><strong class="text-uppercase">Cédula:</strong></div>
              <div class="col-sm-8">{{$usu->usu_cedula}}</div>
            </div>
            <div class="row mt-3">
              <div class="col-sm-4"><strong class="text-uppercase">Correo:</strong></div>
              <div class="col-sm-8">{{$usu->usu_email}}</div>
            </div>
            <div class="row mt-3">
              <div class="col-sm-4"><strong class="text-uppercase">Dirección:</strong></div>
              <div class="col-sm-8">{{$usu->usu_direccion}}</div>
            </div>
            <div class="row mt-3">
              <div class="col-sm-4"><strong class="text-uppercase">Teléfono:</strong></div>
              <div class="col-sm-8">{{$usu->usu_telefono}}</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection