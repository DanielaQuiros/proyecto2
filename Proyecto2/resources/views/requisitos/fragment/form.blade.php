<div class="form-group">
	{!! Form::label('req_descripcion','Descripcion del requisito') !!}
	{!! Form::textarea('req_descripcion',null,['class'=>'form-control']) !!}
</div>

<div class="form-group">
	{!! Form::hidden('ofer_id',$ofer)!!}
</div>

<div class="form-group">
	{!! Form::submit('Guardar',['class'=>'btn btn-dark']) !!}
</div>

