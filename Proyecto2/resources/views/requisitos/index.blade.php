@extends('layout')

@section('content')

	<div>
		<h2>Requisitos
			<a href="{{ route('requisito.create',['id'=>$ofer->id])}}" class="btn btn-dark">Agregar</a>
			<a href="{{ route('ofertas.index')}}" class="btn btn-dark">Ofertas laborales</a>
		</h2>
		<br>
		<p><strong>Oferta Laboral</strong></p>
		<br>
		<table class="table">
			<tr><th>Descripcion de la oferta</th></tr>
			<tbody>
				<tr>
					<td>{{$ofer->ofer_descripcion}}</td>
				</tr>
			</tbody>
		</table>

		<table class="table">
			<tr><th>Descripcion del requisito</th></tr>
			<tbody>
				@foreach($req as $r)
				<tr>
					<td>{{$r->req_descripcion}}</td>
					<td><a href="{{ route('requisito.edit', $r->req_id)}}" class="btn btn-link">Editar</a></td>
					<td>
						<form action="{{ route('requisito.destroy', $r->req_id) }}" method="post"> 
							{{csrf_field()}}
							<input type="hidden" name="_method" value="DELETE">
							<button class="btn btn-link"> Eliminar</button>
						</form>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>

@endsection