@extends('layout')

@section('content')

<div class="column-sm-12">
	<h2>Editar Requisito</h2>
	@include('experiencias.fragment.error')
	
	{!!Form::model($requisito, ['route' => ['requisito.update',$requisito->req_id], 'method' => 'PUT'])!!}
	@include('requisitos.fragment.form')
	{!!Form::close() !!}

	</div>

@endsection