@extends('layout')

@section('content')

<div class="column-sm-8" >
	<h2>Requisitos</h2>
	<table class="table">
		<tr><th>Descripcion del requisito</th></tr>
		<tbody>
			@foreach($requisitos as $r)
			<tr>
				<td>{{$r->req_descripcion}}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
	<br>
	<table class="table">
		<tr><th>Categoria de la oferta</th><th>Descripcion de la oferta</th><th>Vacantes de la oferta</th><th>Fecha de la oferta</th></tr>
		<tbody>
			<tr>
				<td>{{$ofer->ofer_categoria}}</td>
				<td>{{$ofer->ofer_descripcion}}</td>
				<td>{{$ofer->ofer_vacantes}}</td>
				<td>{{$ofer->ofer_fecha}}</td>
			</tr>
		</tbody>
	</table>


</div>

@endsection