@extends('layout')

@section('content')

<div class="column-sm-8">
	@include('curriculum.fragment.error')

	{!!Form::open(['route' => 'requisito.store'])!!}
	@include('requisitos.fragment.form')
	{!!Form::close() !!}
	</div>
@endsection