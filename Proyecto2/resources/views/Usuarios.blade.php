﻿<!DOCTYPE html>
<html>
<head>
	<title>Usuarios</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
</head>
<body>
    
    <div class="container">
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal"> Registrarse </button>
        
        @if(count($errors) > 0)
        
        <div class="alert alert-danger">
            <ul>
            @foreach($errors->all() as $error)
                <li>{{$error}}</li>
            @endforeach
            </ul>
        </div>
        @endif
        
        @if(\Session::has('success'))
            <div class="alert alert-success" role="alert">
                <p> {{\Session::get('success')}} </p>
            </div>
        @endif
    </div>

    <!-- Modal registrar -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Registro de usuario</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
        <form action="{{ action('UsuarioController@store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="modal-body">
                
              <div class="form-group">
                <label>Nombre</label>
                <input type="text" class="form-control" name="usu_nombre" id="usu_nombre">
              </div>
                
              <div class="form-group">
                <label>Apellidos</label>
                <input type="text" class="form-control" name="usu_apellidos" id="usu_apellidos">
              </div>
                
              <div class="form-group">
                <label>Cédula/Cédula Jurídica</label>
                <input type="text" class="form-control" name="usu_cedula" id="usu_cedula">
              </div>
                
              <div class="form-group">
                <label>Correo</label>
                <input type="email" class="form-control" name="usu_email" id="usu_email">
              </div>
                
              <div class="form-group">
                <label>Dirección</label>
                <textarea class="form-control" name="usu_direccion" id="usu_direccion" rows="3"></textarea>
              </div>

              <div class="form-group">
                <label>Teléfono</label>
                <input type="text" class="form-control" name="usu_telefono" id="usu_telefono"></input>
              </div>
                
             <div class="form-group">
                <label>Nombre de Usuario</label>
                <input type="text" class="form-control" name="usu_usuario" id="usu_usuario">
              </div>
                
              <div class="form-group">
                <label>Contraseña</label>
                <input type="password" class="form-control" name="contrasena" id="contrasena">
              </div>
                
              <div class="form-group">
                  <label>Tipo de usuario:</label>
                  <select name="usu_tipo" class="form-control" title="Tipo de usuario">
                      <option value="u">Candidato</option>
                      <option value="e">Empresa</option>
                  </select>
              </div>
                
              <div class="form-group">
                <label>Foto</label>
                <input type="file" class="form-control-file" name="usu_foto" id="usu_foto" accept="image/jpeg,image/gif,image/png">
              </div>
                
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            <button type="submit" class="btn btn-primary">Guardar</button>
          </div>
        </form>
        </div>
      </div>
    </div>
    
    
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    
</body>
</html>