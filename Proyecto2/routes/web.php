<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () 
{
    return view('login');
});

Route::get('usuario/validar', ['as' => 'usuarios.validar', 'uses' => 'UsuarioController@validar']);
Route::get('usuario/logout', ['as' => 'usuario.logout', 'uses' => 'UsuarioController@logout']);
Route::resource('usuario', 'UsuarioController');
Route::resource('curriculum','CurriculumController');
Route::resource('ofertas', 'OfertasController');
Route::resource('experiencia','ExperienciaController');
Route::resource('formacion','FormacionController');
Route::resource('categoria','CategoriasController');
Route::get('grafico', ['as' => 'grafico.index', 'uses' => 'GraficoController@index']);
Route::get('empresa', ['as' => 'reporte.empresa', 'uses' => 'ReportesController@empresa']);
Route::get('vacantesCategoria', ['as' => 'reporte.vacantesCategoria', 'uses' => 'ReportesController@vacantesCategoria']);
Route::get('getEmpresas', ['as' => 'reporte.buscarEmpresa', 'uses' => 'ReportesController@buscarEmpresa']);
Route::get('mostrarEmpresa/{id}', ['as' => 'reporte.mostrarEmpresa', 'uses' => 'ReportesController@mostrarEmpresa']);
Route::get('getAplicadas', ['as' => 'reporte.ofertasAplicadas', 'uses' => 'ReportesController@ofertasAplicadas']);
Route::resource('postulaciones','PostulacionController');
Route::get('inicio', ['as' => 'categoria.inicio', 'uses' => 'CategoriasController@inicio']);
Route::get('aplicadas', ['as' => 'postulaciones.aplicadas', 'uses' => 'PostulacionController@aplicadas']);

Route::get('filtrar', ['as' => 'postulaciones.filtrar', 'uses' => 'PostulacionController@filtrar']);
Route::get('postulantes/{oferta}', ['as' => 'postulaciones.porOferta', 'uses' => 'PostulacionController@porOferta']);
Route::resource('requisito', 'RequisitoController');
//Route::get('requisito/{id}', ['as' => 'requisito.crear', 'uses' => 'RequisitoController@create']);
