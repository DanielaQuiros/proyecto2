<?php

use Illuminate\Database\Seeder;
use App\Experiencia;

class ExperienciaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Experiencia::class,4)->create();
    }
}
