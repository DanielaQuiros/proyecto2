<?php

use Illuminate\Database\Seeder;
use App\Curriculum;

class CurriculumSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Curriculum::class,4)->create();
    }
}
