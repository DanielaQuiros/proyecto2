<?php

use Illuminate\Database\Seeder;
use App\Ofertas;

class OfertasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         factory(Ofertas::class, 10)->create();
    }
}
