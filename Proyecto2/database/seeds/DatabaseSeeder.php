<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsuariosTableSeeder::class);
        $this->call(OfertasTableSeeder::class);
        $this->call(ExperienciaSeeder::class);
        $this->call(FormacionSeeder::class);
        $this->call(CurriculumSeeder::class);
    }
}
