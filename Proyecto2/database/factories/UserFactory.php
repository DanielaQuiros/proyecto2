<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;
use App\Curriculum;
use App\Ofertas;
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'remember_token' => Str::random(10),
    ];
});
$factory->define(Curriculum::class, function (Faker $faker) {
    return [
        'cur_observaciones' => $faker->text(200),
        'cur_fecha' => $faker->date($format='y-m-d', $max='now'),
        'usu_id' =>$faker->integer(),
    ];
});
$factory->define(Ofertas::class, function (Faker $faker) {
    return [
        'ofer_descripcion' => $faker->text(200),
        'ofer_vacantes' => $faker->randomDigit(),
        'ofer_fecha' => $faker->date($format='y-m-d', $max='now'),
        'ofer_contrato' => $faker->text(10),
        'ofer_hora_inicio' => $faker->date($format = 'H:i:s', $max = 'now'),
        'ofer_hora_fin'  => $faker->date($format = 'H:i:s', $max = 'now'),
        'ofer_dia_inicio' => $faker->date($format='y-m-d', $max='now'),
        'ofer_dia_fin' => $faker->date($format='y-m-d', $max='now'),
    ];
});
$factory->define(Experiencia::class, function (Faker $faker)
{
    return [
        'exp_puesto' => $faker->string(30),
        'exp_responsabilidades' => $faker->string(200),
        'exp_fechaI' => $faker->date($format='y-m-d', $max='now'),
        'exp_fechaF' => $faker->date($format='y-m-d', $max='now'),
        'exp_empresa' => $faker->string(40),
        'cur_id' =>$faker->integer(),
    ];
)};
$factory->define(Formacion::class, function (Faker $faker)
{
    return [
        'for_titulo' => $faker->string(50),
        'for_especialidad' => $faker->string(50),
        'for_institucion' => $faker->string(60),
        'for_fecha' => $faker->date($format='y-m-d', $max='now'),
        'cur_id' => $faker->integer(),
)};

$factory->define(Requisito::class,function(Faker $faker)
{
    return [
        'req_descripcion'=> $faker->string(100),
        'ofer_id' => $faker->integer(),
    ];
)};




