<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_usuarios', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id('usu_id');
            $table->string('usu_nombre');
            $table->string('usu_apellidos',60)->nullable();
            $table->string('usu_cedula');
            $table->string('usu_email',60);
            $table->string('usu_direccion',300);
            $table->string('usu_usuario');
            $table->string('usu_contra');
            $table->string('usu_tipo');
            $table->string('usu_foto');
            $table->string('usu_telefono');
            //$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_usuarios');
    }
}
