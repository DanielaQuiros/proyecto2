<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormacionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_formaciones', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('for_id');
            $table->string('for_titulo',50);
            $table->string('for_especialidad',50);
            $table->string('for_institucion',60);
            $table->date('for_fecha');
            $table->integer('cur_id')->unsigned();
            $table->foreign('cur_id')->references('cur_id')->on('tbl_curriculum');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_formaciones');
    }
}
