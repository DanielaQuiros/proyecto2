<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostulacionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_postulacions', function (Blueprint $table) {
            $table->id('pos_id');
            $table->biginteger('pos_usuario')->unsigned();
            $table->foreign('pos_usuario')->references('usu_id')->on('tbl_usuarios');
            $table->biginteger('pos_oferta')->unsigned();
            $table->foreign('pos_oferta')->references('id')->on('tbl_ofertas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_postulacions');
    }
}
