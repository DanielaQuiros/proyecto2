<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExperienciasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_experiencias', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('exp_id');
            $table->string('exp_puesto',30);
            $table->string('exp_responsabilidades',200);
            $table->date('exp_fechaI');
            $table->date('exp_fechaf');
            $table->string('exp_empresa',40);
            $table->integer('cur_id')->unsigned();
            $table->foreign('cur_id')->references('cur_id')->on('tbl_curriculum');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_experiencias');
    }
}
