<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRequisitosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_requisitos', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('req_id');
            $table->string('req_descripcion', 500);
            $table->biginteger('ofer_id')->unsigned();
            $table->foreign('ofer_id')->references('id')->on('tbl_ofertas');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_requisitos');
    }
}
