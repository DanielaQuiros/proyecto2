<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCurriculaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_curriculum', function (Blueprint $table) {
            
            $table->engine = 'InnoDB';
            $table->increments('cur_id');
            $table->mediumText('cur_observaciones');
            $table->date('cur_fecha');
            $table->biginteger('usu_id')->unsigned();
            $table->foreign('usu_id')->references('usu_id')->on('tbl_usuarios');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_curriculum');
    }
}
