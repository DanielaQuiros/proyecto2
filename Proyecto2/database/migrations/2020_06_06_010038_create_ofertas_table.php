<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOfertasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_ofertas', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->integer('ofer_categoria')->unsigned();
            $table->foreign('ofer_categoria')->references('id')->on('tbl_categorias');
            $table->text('ofer_descripcion');
            $table->integer('ofer_vacantes');
            $table->date('ofer_fecha');
            $table->text('ofer_contrato');
            $table->text('ofer_hora_inicio');
            $table->text('ofer_hora_fin');
            $table->text('ofer_dia_inicio');
            $table->text('ofer_dia_fin');
            $table->biginteger('ofer_salario');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_ofertas');
    }
}
