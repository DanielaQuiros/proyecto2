<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Curriculum extends Model
{
    protected $table = 'tbl_curriculum';
	protected $primaryKey = 'cur_id';
	
	/**
	* @var array
    */
    protected $fillable = [
        'cur_observaciones', 'cur_fecha','usu_id'
    ];
}
