<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Requisito extends Model
{
    protected $table = 'tbl_requisitos';
	protected $primaryKey = 'req_id';
	
	/**
	* @var array
    */
    protected $fillable = [
        'req_id', 'req_descripcion','ofer_id',
    ];
}
