<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Requisito;
use App\Http\Requests\RequisitoRequest;
use Illuminate\Support\Facades\DB;
use Session;

class RequisitoController extends Controller
{
    public function index(Request $id)
    {
        Session::put('oferta',$id->id);
        $req = Requisito::where('ofer_id',$id->id)->get();
        $ofer = DB::table('tbl_ofertas')->where('id',$id->id)->first();
        return view('requisitos.index', compact('req'),['ofer'=> $ofer]);
    }
    public function create( Request $id) 
    {
        $ofer = DB::table('tbl_ofertas')->where('id',$id->id)->first();
    	return view('requisitos.create',['ofer'=> $ofer->id]);
    }
    public function store(RequisitoRequest $request)
    {
    	$requisito = new Requisito;
    	$requisito->req_descripcion = $request->req_descripcion;
    	$requisito->ofer_id = $request->ofer_id;
    	$requisito->save();

        $req = Requisito::where('ofer_id',$request->ofer_id)->get();
        $ofer = DB::table('tbl_ofertas')->where('id',$request->ofer_id)->first();

        return view('requisitos.index', compact('req'),['ofer'=> $ofer]);
    }
    public function edit($id)
    {
    	$requisito = Requisito::find($id);

    	return view('requisitos.edit', compact('requisito'),['ofer'=> $requisito->ofer_id]);
    }
    public function update(RequisitoRequest $request, $id)
    {
    	$requisito = Requisito::find($id);
    	$requisito->req_descripcion = $request->req_descripcion;
    	$requisito->ofer_id = $request->ofer_id;
        $requisito->save();

        $req = Requisito::where('ofer_id',$request->ofer_id)->get();
        $ofer = DB::table('tbl_ofertas')->where('id',$request->ofer_id)->first();
        
        return view('requisitos.index', compact('req'),['ofer'=> $ofer]);
    }
    public function show($id)
    {
    	$requisitos = DB::table('tbl_requisitos')->where('ofer_id',$id)->get();
        $ofer = DB::table('tbl_ofertas')->where('id',$id)->first();
    	return view('requisitos.show', compact('requisitos'),['ofer'=> $ofer]);
    }
    public function destroy($id)  
    {
    	$requisito = Requisito::find($id);
    	$requisito->delete();
        $req = Requisito::where('ofer_id',$id)->get();
        $ofer = DB::table('tbl_ofertas')->where('id',Session::get('oferta'))->first();
        
        return view('requisitos.index', compact('req'),['ofer'=> $ofer]);
    }
}
