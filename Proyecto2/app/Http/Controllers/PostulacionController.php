<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Ofertas;
use App\Categorias;
use App\Usuario;
use App\Postulacion;

use Session;

class PostulacionController extends Controller
{
    public function index(){
    	//$ofertas = Ofertas::orderBy('id', 'desc')->paginate();

     $ofertas = DB::table('tbl_usuarios')->join('tbl_categorias', 'cat_usuario', '=', 'usu_id')->Join('tbl_ofertas','tbl_categorias.id','=', 'ofer_categoria')->select('usu_nombre','tbl_ofertas.*')->get();
    	return view('postulaciones.index', compact('ofertas'));

    }

    public function show($id){
	$oferta = Ofertas::find($id);
    $categorias  = Categorias::find($oferta->ofer_categoria);
    $usuario = Usuario::find($categorias->cat_usuario);
	return view('postulaciones.show', compact('oferta','categorias','usuario'));
    }

    public function edit($id){
        $post = Postulacion::where('pos_oferta', '=', $id,'and', 'usu_id', '=' ,Session::get('usuarioActual')->usu_id)->get();
        if(count($post)>0){

        $ofertas = DB::table('tbl_usuarios')->join('tbl_categorias', 'cat_usuario', '=', 'usu_id')->Join('tbl_ofertas','tbl_categorias.id','=', 'ofer_categoria')->select('usu_nombre','tbl_ofertas.*')->get();
        return view('postulaciones.index', compact('ofertas'))->with('info','Imposible aplicar de nuevo');
        }else{
        $postulacion = new Postulacion;
        $postulacion->pos_usuario = Session::get('usuarioActual')->usu_id;
        $postulacion->pos_oferta = $id;
        $postulacion->save();

         $ofertas = DB::table('tbl_usuarios')->join('tbl_categorias', 'cat_usuario', '=', 'usu_id')->Join('tbl_ofertas','tbl_categorias.id','=', 'ofer_categoria')->select('usu_nombre','tbl_ofertas.*')->get();
    	return view('postulaciones.index', compact('ofertas'));
    }
    }

	public function destroy($id){
        $post = Postulacion::where('pos_oferta', '=', $id,'and', 'usu_id', '=' ,Session::get('usuarioActual')->usu_id)->first();
        if($post->pos_id!=null){
            $postulacion = Postulacion::find($post->pos_id);
            $postulacion->delete();
        }
       $ofertas = DB::table('tbl_usuarios')->join('tbl_categorias', 'cat_usuario', '=', 'usu_id')->Join('tbl_ofertas','tbl_categorias.id','=', 'ofer_categoria')->join('tbl_postulacions', 'tbl_ofertas.id', '=', 'pos_oferta')->select('usu_nombre','tbl_ofertas.*')->get(); 
        return view('postulaciones.aplicadas', compact('ofertas')); 
    }

    public function aplicadas(){
        $ofertas = DB::table('tbl_usuarios')->join('tbl_categorias', 'cat_usuario', '=', 'usu_id')->Join('tbl_ofertas','tbl_categorias.id','=', 'ofer_categoria')->join('tbl_postulacions', 'tbl_ofertas.id', '=', 'pos_oferta')->select('usu_nombre','tbl_ofertas.*')->get(); 
    	return view('postulaciones.aplicadas', compact('ofertas'));    	
    }

    public function porOferta($id){
        $Usuarios = Usuario::join('tbl_postulacions', 'usu_id', '=', 'pos_usuario')->where('pos_oferta','=',$id)->get();
        return view('postulaciones.candidatos', compact('Usuarios'));
    }

    public function filtrar(request $request){

      $ofertas = DB::table('tbl_usuarios')->join('tbl_categorias', 'cat_usuario', '=', 'usu_id')->Join('tbl_ofertas','tbl_categorias.id','=', 'ofer_categoria')->where('tbl_usuarios.usu_nombre','like','%'.$request->valor.'%')->orWhere('tbl_categorias.cat_nombre', 'like', '%'.$request->valor.'%')->select('usu_nombre','tbl_ofertas.*')->get();
        return view('postulaciones.index', compact('ofertas'));
    }

}