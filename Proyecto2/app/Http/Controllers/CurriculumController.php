<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Curriculum;
use App\Usuario;
use App\Experiencia;
use App\Formacion;
use App\Http\Requests\CurriculumRequest;
use Illuminate\Support\Facades\DB;
use Session;

class CurriculumController extends Controller
{
    public function index()
    {
        if(Session::get('usuarioActual')->usu_tipo == 'u')
        {
            $exp = [];
            $for = [];
            $cur = DB::table('tbl_curriculum')->where('usu_id',Session::get('usuarioActual')->usu_id)->first();
            if($cur)
            {
                $exp = DB::table('tbl_experiencias')->where('cur_id',$cur->cur_id)->get();
                $for = DB::table('tbl_formaciones')->where('cur_id',$cur->cur_id)->get();
            }
            return view('curriculum.index', compact('cur','exp','for'));
        }
        
    }
    public function create()
    {	    
        return view('curriculum.create');
    }

    public function store(CurriculumRequest $request){
    	$curriculum = new Curriculum;
     	$curriculum->cur_observaciones = $request->cur_observaciones;
     	$curriculum->cur_fecha = $request->cur_fecha;
        $curriculum->usu_id = $request->usu_id;
     	$curriculum->save();
    	return redirect()->route('curriculum.index')->with('info', 'Guardado Actualizado');
    }

    public function edit($id)
    {
    	$curriculum = Curriculum::find($id);
    	return view('curriculum.edit', compact('curriculum'));
    }

    public function update(CurriculumRequest $request, $id)
    {
     	$curriculum = Curriculum::find($id);
     	$curriculum->cur_observaciones = $request->cur_observaciones;
     	$curriculum->cur_fecha = $request->cur_fecha;
        $curriculum->usu_id = $request->usu_id;
     	$curriculum->save();
    	return redirect()->route('curriculum.index')->with('info', 'Curriculum Actualizado');
    }
    public function destroy($id)
    {
    	$curriculum = Curriculum::find($id);
        $exp = DB::table('tbl_experiencias')->where('cur_id',$id)->get();
        $for = DB::table('tbl_formaciones')->where('cur_id',$id)->get();
        foreach($exp as $e)
        {
            $e->delete();
        }
        foreach($for as $f)
        {
            $f->delete();
        }
    	$curriculum->delete();
    	return back()->with('info', 'Curriculum eliminado');
    }
    public function show($id)
    {   
        $exp = [];
        $for = [];
        $cur = DB::table('tbl_curriculum')->where('usu_id',$id)->first();
        $usu = DB::table('tbl_usuarios')->where('usu_id',$id)->first();
        $exp = DB::table('tbl_experiencias')->where('cur_id',$cur->cur_id)->get();
        $for = DB::table('tbl_formaciones')->where('cur_id',$cur->cur_id)->get();

        return view('curriculum.show', compact('for','exp'),['cur'=> $cur, 'usu'=> $usu]);
    }
}
