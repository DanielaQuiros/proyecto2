<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Usuario;
use Illuminate\Support\Facades\DB;
use Session;

class UsuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('login');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'usu_nombre' => 'required', 
            //'usu_apellidos' => 'required',
            'usu_cedula' => 'required',
            'usu_email' => 'required', 
            'usu_direccion' => 'required', 
            'usu_email' => 'required',
            'usu_usuario' => 'required',
            'contrasena' => 'required', 
            'usu_tipo' => 'required',
            'usu_foto' => 'required|image|mimes:jpeg,png,jpg,gif|max:5000',
            'usu_telefono' => 'required',
        ]);
        
        $file = $request->file('usu_foto');
        $file->move(public_path("photos"), $file->getClientOriginalName());
        
        $usuarioModel = new Usuario;
        $usuarioModel->usu_nombre = $request->input('usu_nombre');
        $usuarioModel->usu_apellidos = $request->input('usu_apellidos');
        $usuarioModel->usu_cedula = $request->input('usu_cedula');
        $usuarioModel->usu_email = $request->input('usu_email');
        $usuarioModel->usu_direccion = $request->input('usu_direccion');
        $usuarioModel->usu_contra = $request->input('contrasena');
        $usuarioModel->usu_usuario = $request->input('usu_usuario');
        $usuarioModel->usu_tipo = $request->input('usu_tipo');
        $usuarioModel->usu_foto = $file->getClientOriginalName();
        $usuarioModel->usu_telefono = $request->input('usu_telefono');
        
        $usuarioModel->save();
        
        //return redirect(route('/'))->with('success', 'Usuario guardado con éxito');
        return view('login');
    }
    
    
    public function validar(Request $request)
    {
        $usuarioModel = new Usuario;
        $this->validate($request, [
            'nomUsuario'           => 'required',
            'conUsuario'           => 'required',
        ]);
        $result = DB::table('tbl_usuarios')->where('usu_usuario', $request->input('nomUsuario'))
        ->where('usu_contra',$request->input('conUsuario'))->first();
        if (!is_null($result)) {
            Session::put('usuarioActual', $result);
            return view('welcome');
        }else{
            return redirect()->back()->with('loginError', 'Usuario o contraseña incorrectos');
        }
    }
    
    public function logout(){
        Session::put('usuarioActual', null);
        return view('login');
    }
    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $usu = DB::table('tbl_usuarios')->where('usu_id',$id)->first();
        return view('usuarios.show', ['usu'=> $usu]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'usu_nombre' => 'required', 
            //'usu_apellidos' => 'required',
            'usu_cedula' => 'required',
            'usu_email' => 'required', 
            'usu_direccion' => 'required', 
            'usu_email' => 'required',
            'usu_usuario' => 'required',
            'contrasena' => 'required', 
            'usu_tipo' => 'required',
            'usu_telefono' => 'required'
        ]);
        
        $usuarioModel = Usuario::find($id);
        
        if($request->file('usu_foto') != null){
            $file = $request->file('usu_foto');
            $file->move(public_path("photos"), $file->getClientOriginalName());
            $usuarioModel->usu_foto = $file->getClientOriginalName(); 
        }
        
        $usuarioModel->usu_nombre = $request->input('usu_nombre');
        $usuarioModel->usu_apellidos = $request->input('usu_apellidos');
        $usuarioModel->usu_cedula = $request->input('usu_cedula');
        $usuarioModel->usu_email = $request->input('usu_email');
        $usuarioModel->usu_direccion = $request->input('usu_direccion');
        $usuarioModel->usu_contra = $request->input('contrasena');
        $usuarioModel->usu_usuario = $request->input('usu_usuario');
        $usuarioModel->usu_tipo = $request->input('usu_tipo');
        $usuarioModel->usu_telefono = $request->input('usu_telefono');

        $usuarioModel->save();
        
        return redirect()->back()->with('success', 'Usuario modificado con éxito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
