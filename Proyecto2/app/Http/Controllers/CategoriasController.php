<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CategoriasRequest;
use App\Categorias;
use Session;

class CategoriasController extends Controller
{
      public function index(){
    	//$Categorias = Categorias::orderBy('id', 'desc')->paginate();
        //$idd = Session::get('usuarioActual')->usu_id;
        $Categorias = Categorias::where('cat_usuario','=', Session::get('usuarioActual')->usu_id)->get();
       // dd( $Categorias);
    	return view('categoria.index', compact('Categorias'));
    }

    public function create(){
    	return view('categoria.create');
    }

	public function store(CategoriasRequest $request){
        //return $request;
    	$categoria = new Categorias;
         $categoria->cat_nombre = $request->cat_nombre;
         $categoria->cat_description = $request->cat_description;
         $categoria->cat_usuario = $request->cat_usu;
     	$categoria->save();

    	return redirect()->route('categoria.index')->with('info', 'Guardado Actualizado');
    }


    public function edit($id){
    	$categoria = Categorias::find($id);
    	return view('categoria.edit', compact('categoria'));
    }

     public function update(CategoriasRequest $request, $id){
     	$categoria = Categorias::find($id);
        $categoria->cat_nombre = $request->cat_nombre;
        $categoria->cat_description = $request->cat_description;
        $categoria->cat_usuario = Session::get('usuarioActual')->usu_id;
        $categoria->save();
    	return redirect()->route('categoria.index')->with('info', 'Categoria Actualizado');
    }
    public function show($id){
		$categoria = Categorias::find($id);
    	return view('categoria.show', compact('categoria'));
    }


    public function destroy($id)
    {
        $categoria = Categorias::find($id);
        $categoria->delete();
        return back()->with('info', 'categoria eliminada');
    }

     public function inicio(){
        return view('welcome');
    }
}
