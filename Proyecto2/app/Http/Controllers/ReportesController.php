<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Session;

class ReportesController extends Controller {

    public function empresa()
    {
        $empresas = DB::table('tbl_usuarios')->where('usu_tipo', '=', 'e')->get();
        return view('Reportes/buscarEmpresa', ['empresas' => $empresas]);
    }
    
    public function buscarEmpresa(Request $request)
    {
        $nombre = $request->nombre;
        if($nombre != ''){
            $empresas = DB::table('tbl_usuarios')->where('usu_tipo', '=', 'e')->where('usu_nombre', 'like', $nombre)->get();
        }else{
            $empresas = DB::table('tbl_usuarios')->where('usu_tipo', '=', 'e')->get();
        }
        return view('Reportes/buscarEmpresa', ['empresas' => $empresas]);
    }

    public function mostrarEmpresa($id)
    {
        $empresa = DB::table('tbl_usuarios')->where('usu_id', '=', $id)->first();
        return view('Reportes/mostrarEmpresa', ['usu' => $empresa]);
    }
    
    public function vacantesCategoria(){
        $results = DB::select("SELECT tbl_categorias.cat_nombre, SUM(tbl_ofertas.ofer_vacantes) as total FROM tbl_ofertas join tbl_categorias on tbl_categorias.id = tbl_ofertas.ofer_categoria group by tbl_categorias.cat_nombre");
        
        return view('Reportes/ofertasCategoria', [ 'datos' => $results ] );
    }
    
    public function ofertasAplicadas(){
        $ofertas = DB::table('tbl_usuarios')->join('tbl_categorias', 'cat_usuario', '=', 'usu_id')->Join('tbl_ofertas','tbl_categorias.id','=', 'ofer_categoria')->join('tbl_postulacions', 'tbl_ofertas.id', '=', 'pos_oferta')->select('usu_nombre','tbl_ofertas.*')->get(); 
    	return view('Reportes/ofertasAplicadas', compact('ofertas'));  
    }
}