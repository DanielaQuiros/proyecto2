<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Experiencia;
use App\Curriculum;
use App\Http\Requests\ExperienciaRequest;
use Illuminate\Support\Facades\DB;
use Session;

class ExperienciaController extends Controller
{
    public function index()
    {
    	$experiencia = Experiencia::orderBy('exp_id','DESC')->paginate();
        return view('experiencias.index', compact('experiencia'));
    }
    public function create()
    {
        $c = DB::table('tbl_curriculum')->where('usu_id',Session::get('usuarioActual')->usu_id)->first();
        $cur = $c->cur_id;
        return view('experiencias.create',compact('cur'));	
    }
    public function store(ExperienciaRequest $request){
    	$experiencia = new Experiencia;
     	$experiencia->exp_puesto = $request->exp_puesto;
     	$experiencia->exp_responsabilidades = $request->exp_responsabilidades;
     	$experiencia->exp_fechaI = $request->exp_fechaI;
     	$experiencia->exp_fechaf = $request->exp_fechaf;
        $experiencia->exp_empresa = $request->exp_empresa;
        $experiencia->cur_id = $request->cur_id;
     	$experiencia->save();
    	return redirect()->route('curriculum.index')->with('info', 'Experiencia Guardada');
    }

    public function edit($id)
    {
    	$experiencia = Experiencia::find($id);
        $c = DB::table('tbl_curriculum')->where('usu_id',Session::get('usuarioActual')->usu_id)->first();
        $cur = $c->cur_id;
        return view('experiencias.edit', compact('experiencia','cur'));
    }

    public function update(ExperienciaRequest $request, $id)
    {
     	$experiencia = Experiencia::find($id);
     	$experiencia->exp_puesto = $request->exp_puesto;
     	$experiencia->exp_responsabilidades = $request->exp_responsabilidades;
     	$experiencia->exp_fechaI = $request->exp_fechaI;
     	$experiencia->exp_fechaf = $request->exp_fechaf;
        $experiencia->exp_empresa = $request->exp_empresa;
        $experiencia->cur_id = $request->cur_id;
     	$experiencia->save();
    	return redirect()->route('curriculum.index')->with('info', 'Experiencia Actualizada');
    }
    public function destroy($id)
    {
    	$experiencia = Experiencia::find($id);
    	$experiencia->delete();
    	return back()->with('info', 'Experiencia eliminada');
    }
}
