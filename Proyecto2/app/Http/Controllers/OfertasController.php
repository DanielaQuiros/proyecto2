<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\ofertasRequest;
use Illuminate\Support\Facades\DB;
use App\Ofertas;
use App\Categorias;
use App\Usuario;
use App\Postulacion;
use App\Requisito;
use Session;

class OfertasController extends Controller
{
    public function index(){
    	//$ofertas = Ofertas::orderBy('id', 'desc')->paginate();
        $ofertas = DB::table('tbl_usuarios')->join('tbl_categorias', 'cat_usuario', '=', 'usu_id')->Join('tbl_ofertas','tbl_categorias.id','=', 'ofer_categoria')->where('cat_usuario','=', Session::get('usuarioActual')->usu_id)->get();
    	return view('ofertas.index', compact('ofertas'));
    }

    public function create(){
        $cats = Categorias::where('cat_usuario','=', Session::get('usuarioActual')->usu_id)->get();
        $categorias = [];
        foreach ($cats as $cat) {
            $categorias = $categorias + [ $cat->id => $cat->cat_nombre];
        }
    	return view('ofertas.create',compact('categorias'));
    }

	public function store(ofertasRequest $request){
    	 $oferta = new Ofertas;
         $oferta->ofer_categoria = $request->ofer_categoria;
         $oferta->ofer_descripcion = $request->ofer_descripcion;
         $oferta->ofer_vacantes = $request->ofer_vacantes;
         $oferta->ofer_fecha = $request->ofer_fecha;
         $oferta->ofer_contrato = $request->ofer_contrato;
         $oferta->ofer_hora_inicio = $request->ofer_hora_inicio;
         $oferta->ofer_hora_fin = $request->ofer_hora_fin;
         $oferta->ofer_dia_inicio = $request->ofer_dia_inicio;
         $oferta->ofer_dia_fin = $request->ofer_dia_fin;
         $oferta->ofer_salario = $request->ofer_salario;
     	 $oferta->save();
    	return redirect()->route('ofertas.index')->with('info', 'Guardado Actualizado');
    }


    public function edit($id){
    	$oferta = Ofertas::find($id);
        //$cats = Categorias::get();
        $cats = Categorias::where('cat_usuario','=', Session::get('usuarioActual')->usu_id)->get();
        $categorias = [];
        foreach ($cats as $cat) {
            $categorias = $categorias + [ $cat->id => $cat->cat_nombre];
        }
    	return view('ofertas.edit', compact('oferta','categorias'));
    }

     public function update(ofertasRequest $request, $id){
     	$oferta = Ofertas::find($id);
         $oferta->ofer_categoria = $request->ofer_categoria;
         $oferta->ofer_descripcion = $request->ofer_descripcion;
         $oferta->ofer_vacantes = $request->ofer_vacantes;
         $oferta->ofer_fecha = $request->ofer_fecha;
         $oferta->ofer_contrato = $request->ofer_contrato;
         $oferta->ofer_hora_inicio = $request->ofer_hora_inicio;
         $oferta->ofer_hora_fin = $request->ofer_hora_fin;
         $oferta->ofer_dia_inicio = $request->ofer_dia_inicio;
         $oferta->ofer_dia_fin = $request->ofer_dia_fin;
         $oferta->ofer_salario = $request->ofer_salario;
         $oferta->save();
    	return redirect()->route('ofertas.index')->with('info', 'Ofera Actualizado');
    }
    public function show($id){
        $categorias = [];
        $usuario  = [];
        $oferta = Ofertas::find($id);
        if($oferta){
            $categorias  = Categorias::find($oferta->ofer_categoria);
            $usuario = Usuario::find($categorias->cat_usuario);
        }
        return view('ofertas.show', compact('oferta','categorias','usuario'));
    }


    public function destroy($id)
    {
        $oferta = Ofertas::find($id);
        $req = Requisito::where('ofer_id',$id)->get();
        foreach($req as $r)
        {
            $r->delete();
        }
        
        $oferta->delete();

        return back()->with('info', 'Oferta eliminada');
    }
}
