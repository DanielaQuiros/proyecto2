<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Formacion;
use App\Http\Requests\FormacionRequest;
use Illuminate\Support\Facades\DB;
use Session;

class FormacionController extends Controller
{
    public function index()
    {
    	$formacion = Formacion::orderBy('for_id','DESC')->paginate();
        return view('formaciones.index', compact('formacion'));
    }
    public function create()
    {
        $c = DB::table('tbl_curriculum')->where('usu_id',Session::get('usuarioActual')->usu_id)->first();
        $cur = $c->cur_id;
    	return view('formaciones.create', ["cur" => $cur]);
    }
    public function store(FormacionRequest $request){
    	$formacion = new Formacion;
     	$formacion->for_titulo = $request->for_titulo;
     	$formacion->for_especialidad = $request->for_especialidad;
     	$formacion->for_institucion = $request->for_institucion;
     	$formacion->for_fecha = $request->for_fecha;
        $formacion->cur_id = $request->cur_id;
     	$formacion->save();
    	return redirect()->route('curriculum.index')->with('info', 'Formacion Guardada');
    }

    public function edit($id)
    {
    	$formacion = Formacion::find($id);
        $c = DB::table('tbl_curriculum')->where('usu_id',Session::get('usuarioActual')->usu_id)->first();
        $cur = $c->cur_id;
    	return view('formaciones.edit', compact('formacion'), ["cur" => $cur]);
    }

    public function update(FormacionRequest $request, $id)
    {
     	$formacion = Formacion::find($id);
     	$formacion->for_titulo = $request->for_titulo;
     	$formacion->for_especialidad = $request->for_especialidad;
     	$formacion->for_institucion = $request->for_institucion;
     	$formacion->for_fecha = $request->for_fecha;
        $formacion->cur_id = $request->cur_id;
     	$formacion->save();
    	return redirect()->route('curriculum.index')->with('info', 'Formacion Actualizada');
    }
    public function destroy($id)
    {
    	$formacion= Formacion::find($id);
    	$formacion->delete();
    	return back()->with('info', 'Formacion eliminada');
    }
}
