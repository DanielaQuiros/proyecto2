<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FormacionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'for_titulo' => 'required',
            'for_institucion' => 'required',
            'for_especialidad' => 'required',
            'for_fecha' => 'required',
            'cur_id' => 'required',
        ];
    }
}
