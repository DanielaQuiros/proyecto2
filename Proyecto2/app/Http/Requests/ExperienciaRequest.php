<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ExperienciaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'exp_puesto' => 'required',
            'exp_responsabilidades' => 'required',
            'exp_fechaI' => 'required',
            'exp_fechaf' => 'required',
            'exp_empresa' => 'required',
            'cur_id' => 'required',
        ];
    }
}
