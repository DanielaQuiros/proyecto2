<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ofertasRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        'ofer_descripcion' => 'required',
        'ofer_vacantes'    => 'required',
        'ofer_fecha'       => 'required',
        'ofer_contrato'    => 'required',
        'ofer_hora_inicio' => 'required',
        'ofer_hora_fin'    => 'required',
        'ofer_dia_inicio'  => 'required',
        'ofer_dia_fin'     => 'required',
        'ofer_categoria'   => 'required',
        'ofer_salario'     => 'required',
        ];
    }
}
