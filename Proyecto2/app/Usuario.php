<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    public $table = 'tbl_usuarios';
    public $timestamps = false;
    protected $primaryKey = 'usu_id';
}
