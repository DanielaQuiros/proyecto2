<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categorias extends Model
{
    protected $table = 'tbl_categorias';
	protected $primaryKey = 'id';

	    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'cat_nombre',
        'cat_description',
    ];
}
