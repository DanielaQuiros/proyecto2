<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Ofertas extends Model
{
    protected $table = 'tbl_ofertas';
	protected $primaryKey = 'id';
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ofer_descripcion',
        'ofer_vacantes',
        'ofer_fecha',
        'ofer_contrato',
        'ofer_hora_inicio',
        'ofer_hora_fin',
        'ofer_dia_inicio',
        'ofer_dia_fin',
        'ofer_salario',
    ];


    //scope

    public function scopeNombre($query, $nombre){
        if($nombre){
            return $query->join('tbl_categorias', 'ofer_categoria','=','tbl_categorias.id')->where('')->get();
        }

    }
}
