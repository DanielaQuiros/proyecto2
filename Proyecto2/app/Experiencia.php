<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Experiencia extends Model
{
    protected $table = 'tbl_experiencias';
	protected $primaryKey = 'exp_id';
	
	/**
	* @var array
    */
    protected $fillable = [
        'exp_puesto','exp_responsabilidades', 'exp_fechaI','exp_fechaf','exp_empresa','cur_id'
    ];
}
