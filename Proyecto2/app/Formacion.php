<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Formacion extends Model
{
    protected $table = 'tbl_formaciones';
	protected $primaryKey = 'for_id';
	
	/**
	* @var array
    */
    protected $fillable = [
        'for_titulo','for_especialidad','for_institucion','for_fecha','cur_id'
    ];
}
