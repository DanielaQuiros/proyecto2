<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Postulacion extends Model
{
    protected $table = 'tbl_postulacions';
	protected $primaryKey = 'pos_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'pos_usuario',
        'pos_oferta',
    ];
}
